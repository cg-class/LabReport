

\newpage 

## Practical 1 
### Simple DDA  
*Objective*: To draw a line by taking initial and final point as input using simple dda algorithm  
*Input*: Click with mouse to draw line.  
*Code*:  
```c

// Algorithm
std::vector<std::pair<int, int>> dda_simple(std::pair<int, int> const &start,
                                            std::pair<int, int> const &end) {
  int dx = end.first - start.first;
  int dy = end.second - start.second;
  int e = std::abs(dx) < std::abs(dy) ? std::abs(dy) : std::abs(dx);
  double incX = static_cast<double>(dx) / e;
  double incY = static_cast<double>(dy) / e;
  std::vector<std::pair<int, int>> res;
  res.reserve(e);
  res.push_back(start);
  double x = start.first;
  double y = start.second;
  for (int i = 0; i < e; ++i) {
    x += incX;
    y += incY;
    res.push_back(
        {static_cast<int>(std::round(x)), static_cast<int>(std::round(y))});
  }
  return res;
}
```
*Output*:  
![Simple DDA Output](images/line_dda_sim.png){width=520px height=549px}  

\newpage

## Practical 2  
### Symmetrical DDA  
*Objective*: To draw a line by taking initial and final point as input using symmetric dda algorithm  
*Input*: Click with mouse to draw line.  
*Code*:  
```c
std::vector<std::pair<int, int>> dda_sym(std::pair<int, int> const &start,
                                         std::pair<int, int> const &end) {
  int dx = end.first - start.first;
  int dy = end.second - start.second;
  int e = std::abs(dx) < std::abs(dy) ? std::abs(dy) : std::abs(dx);
  e = 1 << static_cast<int>(std::floor(log2(e)) + 1);
  double incX = static_cast<double>(dx) / e;
  double incY = static_cast<double>(dy) / e;
  std::vector<std::pair<int, int>> res;
  res.reserve(e);
  res.push_back(start);
  double x = start.first;
  double y = start.second;
  for (int i = 0; i < e; ++i) {
    x += incX;
    y += incY;
    res.push_back(
        {static_cast<int>(std::round(x)), static_cast<int>(std::round(y))});
  }
  return res;
}
```
*Output*:  
![Symmetrical DDA Output](images/line_dda_sym.png){width=520px height=549px}  

\newpage

## Practical 3  
### Bresenham  
*Objective*: To draw a line by taking initial and final point as input using bresenham algorithm  
*Input*: Click with mouse to draw line.  
*Code*:  
```c
template <bool, typename _T>
std::pair<_T, _T> make_pair(_T const &first, _T const &second) {
  return std::pair<_T, _T>(second, first);
}
template <typename _T>
std::pair<_T, _T> make_pair(_T const &first, _T const &second) {
  return std::pair<_T, _T>(first, second);
}

std::vector<std::pair<int, int>> bresenham(std::pair<int, int> const &start,
                                           std::pair<int, int> const &end) {
  int dx = end.first - start.first;
  int dy = end.second - start.second;
  int absDx = std::abs(dx);
  int absDy = std::abs(dy);
  auto [x, y] = start;
  auto [min, max] = std::minmax(absDx, absDy);
  int posInc = (min - max) << 1;
  int negInc = min << 1;
  int initD = (min << 1) - max;
  int incX = (dx < 0) ? -1 : 1;
  int incY = (dy < 0) ? -1 : 1;
  std::vector<std::pair<int, int>> res;
  res.reserve(max + 1);
  res.push_back(start);
  std::pair<int, int> tmp;
  bool rev = false;
  if (absDy > absDx) {
    std::swap(x, y);
    std::swap(incX, incY);
    rev = true;
  }
  for (int i = 0; i < max; ++i) {
   x += incX;
    if (initD >= 0) {
      y += incY;
      initD += posInc;
    } else {
      initD += negInc;
    }
    if (rev) {
      res.push_back(make_pair<true>(x, y));
    } else {
      res.push_back(make_pair(x, y));
    }
  }
  return res;
}
```
*Output*:  
![Bresenham LDA Output](images/line_bresen.png){width=520px height=549px}  

\newpage

## Practical 4  
### Mid-Point LDA  
*Objective*: To draw a line by taking initial and final point as input using mid-point algorithm  
*Input*: Click with mouse to draw line.  
*Code*:
```c
template <bool, typename _T>
std::pair<_T, _T> make_pair(_T const &first, _T const &second) {
  return std::pair<_T, _T>(second, first);
}
template <typename _T>
std::pair<_T, _T> make_pair(_T const &first, _T const &second) {
  return std::pair<_T, _T>(first, second);
}

std::vector<std::pair<int, int>> midpoint(std::pair<int, int> const &start,
                                           std::pair<int, int> const &end) {
  int dx = end.first - start.first;
  int dy = end.second - start.second;
  int absDx = std::abs(dx);
  int absDy = std::abs(dy);
  auto [x, y] = start;
  auto [min, max] = std::minmax(absDx, absDy);
  int posInc = (min - max) << 1;
  int negInc = min << 1;
  int initD = (min << 1) - max;
  int incX = (dx < 0) ? -1 : 1;
  int incY = (dy < 0) ? -1 : 1;
  std::vector<std::pair<int, int>> res;
  res.reserve(max + 1);
  res.push_back(start);
  std::pair<int, int> tmp;
  bool rev = false;
  if (absDy > absDx) {
    std::swap(x, y);
    std::swap(incX, incY);
    rev = true;
  }
  for (int i = 0; i < max; ++i) {
   x += incX;
    if (initD >= 0) {
      y += incY;
      initD += posInc;
    } else {
      initD += negInc;
    }
    if (rev) {
      res.push_back(make_pair<true>(x, y));
    } else {
      res.push_back(make_pair(x, y));
    }
  }
  return res;
}
```
*Output*:  
![MidPoint LDA Output](images/line_midpoint.png){width=520px height=549px}  

\newpage

## Practical 5  
### Patterned Lines  
*Objective*: To draw a patterned line using any line drawing algorithm  
*Input*: Enter 'p' followed by the pattern in binary. After that enter 'ESC'. Click with mouse to draw line.  
*Code*:  
```c
// Driver Code
#ifdef LINE
const std::array names = {"DDA Simple", "DDA Sym", "Bresenham", "MidPoint"};
const std::array funcs = {dda_simple, dda_sym, bresenham, bresenham};
#endif
#ifdef CIRCLE
const std::array names = {"MidPoint Circle Fill", "MidPoint Circle"};
const std::array funcs = {midcircle_fill, midcircle};
#endif
#ifdef ELLIPSE
const std::array names = {"MidPoint Ellipse"};
const std::array funcs = {midellipse};
#endif
enum struct KeyState_ {
  PATTERN = 'p',
  RGB = 'r',
  NORMAL = 'n',
  INPUT = 'i',
};
KeyState_ state = KeyState_::NORMAL;

std::vector<int> windows;
const int winX = 500, winY = 500;

std::vector<std::pair<int, int>> vertices = {};
std::pair<std::pair<int, int>, std::pair<int, int>> clippedRect;
std::stringstream input_buffer;
std::bitset<16> pattern = 0xffff;
float pointSize = 2;
std::array<short, 3> rgb = {255, 255, 255};
int pattCount = 0;
bool fill = false;

// populates vertices to draw
auto raster = [old = std::make_pair(0, 0),
               latch = 0](std::pair<int, int> const &point,
                          decltype(bresenham) *_Func) mutable {
  if (latch % 2) {
    vertices = _Func(old, point);
    glutPostRedisplay();
  } else {
    old = point;
  }
  ++latch;
};

void display() {
  glColor3f(1, 1, 1);
  glRasterPos2i(winX - 400, winY - 50);
  glutBitmapString(
      GLUT_BITMAP_HELVETICA_18,
      reinterpret_cast<const unsigned char *>(
          fmt::format("R: {}, G: {}, B: {}", rgb[0], rgb[1], rgb[2]).c_str()));
  glutBitmapString(GLUT_BITMAP_HELVETICA_18,
                   reinterpret_cast<const unsigned char *>(
                       fmt::format("  {}", pattern.to_string()).c_str()));
  glColor3f(rgb[0] / 255., rgb[1] / 255., rgb[2] / 255.);
  glPointSize(pointSize);
  glBegin(GL_POINTS);
  int i = 0;
  for (auto const &iter : vertices) {
    if (pattern[i % 8]) {
      glVertex2i(iter.first, iter.second);
    }
    ++i;
  }
  vertices.clear();
  glEnd();
  glutSwapBuffers();
}

void glInit() {
  glClear(GL_COLOR_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, winX, winY, 0, 0, 1.0);
}

template <typename _T> void mouse(int button, int state, int x, int y) {
  switch (button) {
  case GLUT_LEFT_BUTTON:
    if (state == GLUT_DOWN) {
      glutSetWindow(windows[_T::value]);
      raster({x, y}, funcs[_T::value]);
    }
    break;
  }
}

template <typename _T> void handle_exit() {
  switch (state) {
  case KeyState_::INPUT: {
    int a, b;
    input_buffer >> a >> b;
    raster({a, b}, funcs[_T::value]);
    input_buffer >> a >> b;
    raster({a, b}, funcs[_T::value]);
    break;}
  case KeyState_::PATTERN: {
    std::string s;
    input_buffer >> s;
    if (s.size() > 16)
      s.erase(17, std::string::npos);
    pattern = decltype(pattern)(s);
    break;
  }
  case KeyState_::RGB: {
    short r, g, b;
    input_buffer >> r >> g >> b;
    rgb = {r, g, b};
    glutPostRedisplay();
    break;
  }
  case KeyState_::NORMAL:
    break;
  }
  input_buffer.clear();
}

template <typename _T> void handle_input(unsigned char const &key) {
  switch (key) {
  case 13:
    std::cout << '\n';
    input_buffer.put('\n');
    break;
  case 8: {
    std::cout << "\033[D\033[K";
    input_buffer.get();
    break;
  }
  default: {
    std::cout << key << std::flush;
    input_buffer << key;
    break;
  }
  }
  std::cout.flush();
}

template <typename _T> void keyboard(unsigned char key, int x, int y) {
  glutSetWindow(windows[_T::value]);
  // std::cout << "Key: " << static_cast<short>(key) << std::endl;
  if (key == '\E') {
    std::cout << "Normal Mode" << std::endl;
    handle_exit<_T>();
    state = KeyState_::NORMAL;
    return;
  }
  if (state != KeyState_::NORMAL) {
    handle_input<_T>(key);
  }
  switch (key) {
  case 'p':
    state = KeyState_::PATTERN;
    std::cout << "Pattern Mode" << std::endl;
    pattern = 0;
    break;
  case 'r':
    state = KeyState_::RGB;
    std::cout << "RGB Mode" << std::endl;
    rgb = {0, 0, 0};
    break;
  case 'f':
    std::cout << "Toggle Fill Mode" << std::endl;
    fill = !fill;
    break;
  case 'i':
    state = KeyState_::INPUT;
    std::cout << "Input Data" << std::endl;
    break;
  }
}

template <int N> struct WinGen {
  enum { value = N - 1 };
  static void gen() {
    WinGen<N - 1>::gen();
    int win = glutCreateWindow(names[value]);
    glutSetWindow(win);
    windows.push_back(win);
    glutDisplayFunc(display);
    glutMouseFunc(mouse<WinGen<N>>);
    glutKeyboardUpFunc(keyboard<WinGen<N>>);
    glInit();
  }
};
template <> struct WinGen<0> {
  static void gen() { return; }
};

int main(int argc, char **args) {
  glutInit(&argc, args);
  glutInitDisplayMode(GLUT_DOUBLE);
  glutInitWindowSize(winX, winY);
  glutInitWindowPosition(0, 0);

  WinGen<names.size()>::gen();

  std::cerr << glGetString(GL_VERSION) << std::endl;
  glutMainLoop();
  return 0;
}
```
*Output*:  
![Patterned Lines Output](images/line_pattern.png){width=520px height=549px}  

\newpage

## Practical 6  
### Circle Drawing  
*Objective*: To draw a circle using midpoint circle drawing algorithm  
*Input*: Click with mouse to draw a circle.  
*Code*:  
```c
std::vector<std::pair<int, int>> midcircle(std::pair<int, int> const &center,
                                           std::pair<int, int> const &end) {
  std::vector<std::pair<int, int>> res;
  std::pair<int, int> r2 = {std::pow(end.first - center.first, 2),
                            std::pow(end.second - center.second, 2)};
  double r = std::sqrt(static_cast<double>(r2.first + r2.second));
  double p0, p;
  double x = 0.0, y = r;
  p0 = 1 - r;
  while (x <= y) {

    double xn = 0.0 - x, yn = 0.0 - y;
    res.emplace_back(x + center.first, y + center.second);
    res.emplace_back(xn + center.first, yn + center.second);
    res.emplace_back(y + center.first, x + center.second);
    res.emplace_back(yn + center.first, xn + center.second);
    res.emplace_back(y + center.first, xn + center.second);
    res.emplace_back(x + center.first, yn + center.second);
    res.emplace_back(yn + center.first, x + center.second);
    res.emplace_back(xn + center.first, y + center.second);
    if (p0 >= 0) {
      x = x + 1;
      y = y - 1;
      p = p0 + 2 * (x - y) + 1;
    } else {
      x = x + 1;
      p = p0 + 2 * x + 1;
    }
    p0 = p;
  }
  return res;
}
``` 
*Output*:  
![Patterned Lines Output](images/circle.png){width=520px height=549px}  

\newpage

## Practical 7  
### Ellipse Drawing  
*Objective*: To draw a Ellipse using midpoint Ellipse drawing algorithm  
*Input*: Enter 'i', followed by center and semi-axes lengths. Then press 'ESC' to draw.  
*Code*:  
```c
using point_t = std::pair<int, int>;
std::vector<point_t> midellipse(point_t const &center,
                                point_t const &semi_axes) {
  std::vector<point_t> trajectory_points;
  std::ptrdiff_t x = semi_axes.first, y = 0;

  long long int const t1 = semi_axes.first * semi_axes.first;
  long long int const t4 = semi_axes.second * semi_axes.second;
  long long int t2, t3, t5, t6, t8, t9;
  t2 = 2 * t1, t3 = 2 * t2;
  t5 = 2 * t4, t6 = 2 * t5;
  long long int const t7 = semi_axes.first * t5;
  t8 = 2 * t7, t9 = 0;

  long long int d1, d2;
  d1 = t2 - t7 + t4 / 2, d2 = t1 / 2 - t8 + t5;

  while (d2 < 0) {
    trajectory_points.push_back({x, y});
    y += 1;
    t9 += t3;
    if (d1 < 0) {
      d1 += t9 + t2;
      d2 += t9;
    } else {
      x -= 1;
      t8 -= t6;
      d1 += t9 + t2 - t8;
      d2 += t5 + t9 - t8;
    }
  }
  while (x >= 0) {
    trajectory_points.push_back({x, y});
    x -= 1;
    t8 -= t6;
    if (d2 < 0) {
      y += 1;
      t9 += t3;
      d2 += t5 + t9 - t8;
    } else {
      d2 += t5 - t8;
    }
  }
  std::vector<point_t> pts;
  std::for_each(trajectory_points.begin(), trajectory_points.end(),
                [&pts](auto &p) {
                  if (p.first != 0)
                    pts.push_back({p.first * -1, p.second});
                  if (p.second != 0)
                    pts.push_back({p.first, p.second * -1});
                  if (p.first != 0 && p.second != 0)
                    pts.push_back({p.first * -1, p.second * -1});
                });
  trajectory_points.insert(trajectory_points.end(), pts.begin(), pts.end());
  std::for_each(trajectory_points.begin(), trajectory_points.end(),
                [&center](auto &p) {
                  p.first += center.first;
                  p.second += center.second;
                });

  return trajectory_points;
}
``` 
*Output*:  
![Patterned Lines Output](images/ellipse.png){width=520px height=549px}  

\newpage

## Practical 8  
### Seed Filling Algoritms  
*Objective*: To draw filled shapes using boundary fill algorithm  
*Input*: Press 'f' to toggle fill mode. Press 'r' followed by the rgb values to select colors. Create shape as described in previous experiments.  
*Code*:  
```c
bool check(Color x,Color y){
    if(x.r==y.r and x.b==y.b and  x.g==y.g)
        return true;
    return false;
}
void boundaryFill(int x, int y, Color fill_color, Color boundary_color) {
  Color current = getColor(x, y);

  if (!check(current, boundary_color) and !check(current, fill_color)) {
    setColor(x, y, fill_color);
    boundaryFill(x - 1, y, fill_color, boundary_color);
    boundaryFill(x, y - 1, fill_color, boundary_color);
    boundaryFill(x + 1, y, fill_color, boundary_color);
    boundaryFill(x, y + 1, fill_color, boundary_color);
  }
}
``` 
*Output*:  
![Boundary Fill](images/floodfill.png){width=520px height=549px}  

*Objective*: To draw filled shapes using flood fill algorithm  
*Input*: Press 'f' to toggle fill mode. Press 'r' followed by the rgb values to select colors. Create shape as described in previous experiments.  
*Code*:  
```c
bool check(Color x,Color y){
    if(x.r==y.r and x.b==y.b and  x.g==y.g)
        return true;
    return false;
}
void floodFill(int x,int y,Color fill_color,Color old_color){
    Color current=getColor(x,y);

    if(check(current,old_color) ){
        setColor(x,y,fill_color);
        floodFill(x-1,y,fill_color,old_color);
        floodFill(x,y-1,fill_color,old_color);
        floodFill(x+1,y,fill_color,old_color);
        floodFill(x,y+1,fill_color,old_color);
    }
}
``` 
*Output*:  
![Flood Fill](images/floodfill.png){width=520px height=549px}  

\newpage

## Practical 9  
### Elementary Geometric Transforms  
*Objective*: To draw a circle using any circle drawing algorithm  
*Input*: Answer questions asked by the program in stdin.  
*Code*:  
```c
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <bits/stdc++.h>
using namespace std;

vector<vector<float>> multiplication(vector<vector<float>> mat1,
                                     vector<vector<float>> mat2) {
  int p = mat1.size();
  int q = mat2[0].size();
  int n = mat1[0].size();
  vector<vector<float>> ans(p, vector<float>(q, 0));
  for (int i = 0; i < p; i++) {
    for (int j = 0; j < q; j++) {
      for (int k = 0; k < n; k++) {
        ans[i][j] += mat1[i][k] * mat2[k][j];
      }
    }
  }
  return ans;
}

vector<vector<float>> transfromation(vector<vector<float>> &mat) {

  vector<vector<float>> ans = mat;
  char ch;
  cout << "Do you want to continue" << endl;
  cin >> ch;
  while (ch == 'y') {
    char choice;
    cout << "For translation press -> t " << endl;
    cout << "For rotation press -> r " << endl;
    cout << "For scaling  press -> s " << endl;
    cin >> choice;
    if (choice == 't') {
      float tx, ty;
      cout << "Enter tx ,ty " << endl;
      cin >> tx >> ty;
      vector<vector<float>> tmat(3, vector<float>(3, 0));
      tmat[0][0] = 1;
      tmat[0][2] = tx;
      tmat[1][2] = ty;
      tmat[1][1] = 1;
      tmat[2][2] = 1;
      ans = multiplication(tmat, ans);
    } else if (choice == 'r') {
      float ang;
      cout << "Enter angle of rotation" << endl;
      cin >> ang;
      ang *= 0.0174533; // converting in radians
      vector<vector<float>> rmat(3, vector<float>(3, 0));
      rmat[0][0] = cos(ang);
      rmat[0][1] = -1 * sin(ang);
      rmat[0][2] = 0;
      rmat[1][0] = sin(ang);
      rmat[1][1] = cos(ang);
      rmat[1][2] = 0;
      rmat[2][0] = 0;
      rmat[2][1] = 0;
      rmat[2][2] = 1;
      ans = multiplication(rmat, ans);
    } else {
      float sx, sy;
      cout << "Enter sx ,sy " << endl;
      cin >> sx >> sy;
      vector<vector<float>> smat(3, vector<float>(3, 0));
      smat[0][0] = sx;
      smat[1][1] = sy;
      smat[2][2] = 1;
      ans = multiplication(smat, ans);
    }
    cout << "Do you want to continue" << endl;
    cin >> ch;
  }

  return ans;
}

void display(void) {
  float xi, yi, xf, yf, dx, dy;
  float x_inc, y_inc;
  vector<vector<float>> mat(3);
  cout << "Enter the starting point" << endl;
  cin >> xi >> yi;
  cout << "Enter the final point" << endl;
  cin >> xf >> yf;
  dx = xf - xi;
  dy = yf - yi;
  float steps = max(abs(dx), abs(dy));
  x_inc = dx / steps;
  y_inc = dy / steps;
  mat[0].push_back(xi);
  mat[1].push_back(yi);
  mat[2].push_back(1);
  glBegin(GL_POINTS);
  glVertex2i(round(xi), round(yi));
  for (int i = 1; i <= steps; i++) {
    xi += x_inc;
    yi += y_inc;
    mat[0].push_back(round(xi));
    mat[1].push_back(round(yi));
    mat[2].push_back(round(1));
    glVertex2i(round(xi), round(yi));
  }
  vector<vector<float>> ans = transfromation(mat);
  int s = ans[0].size();
  glColor3f(1, 0, 1);
  for (int i = 0; i < s; i++) {
    glVertex2i(round(ans[0][i]), round(ans[1][i]));
  }
  glEnd();
  glFlush();
}

void init() {
  glClear(GL_COLOR_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0.0, 700.0, 0, 700.0, 1.0, -1.0);
}

int main(int argc, char **argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE);
  glutInitWindowSize(700, 700);
  glutInitWindowPosition(0, 0);
  glutCreateWindow("Window 1");
  init();
  glutDisplayFunc(display);
  glutMainLoop();
  return 0;
}
``` 
*Output*:  
![2D Transform Output](images/transform_out.png)  

\newpage

## Practical 10
### Clipped Windows and Views
*Objective*: To clip a line using the Cohen-Sutherland algorithm  
*Input*: Click with mouse to draw the line.  
*Code*:  
```c
// Driver Code 
#ifdef SUTHERLAND
const std::array names = {"sutherland"};
const std::array funcs = {sutherland};
#endif

std::vector<int> windows;
const int winX = 1000, winY = 1000;
const int pointSize = 2;
std::vector<std::pair<int, int>> vertices;
std::pair<std::pair<int, int>, std::pair<int, int>> clippedRect;

void display() {
  glColor3f(1, 1, 1);
  glRasterPos2i(winX - 400, winY - 50);
  glPointSize(pointSize);
  glBegin(GL_POINTS);
  for (auto const &iter : vertices) {
    glVertex2i(iter.first, iter.second);
  }
  vertices.clear();
  glEnd();
  glutSwapBuffers();
}

void glInit() {
  glClear(GL_COLOR_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, winX, winY, 0, 0, 1.0);
}

template <int N> struct WinGen {
  enum { value = N - 1 };
  static void gen() {
    WinGen<N - 1>::gen();
    int win = glutCreateWindow(names[value]);
    glutSetWindow(win);
    windows.push_back(win);
    glutDisplayFunc(display);
    // glutMouseFunc(mouse<WinGen<N>>);
    // glutKeyboardUpFunc(keyboard<WinGen<N>>);
    glInit();
  }
};
template <> struct WinGen<0> {
  static void gen() { return; }
};

int main(int argc, char **args) {
  glutInit(&argc, args);
  glutInitDisplayMode(GLUT_DOUBLE);
  glutInitWindowSize(winX, winY);
  glutInitWindowPosition(0, 0);

  WinGen<names.size()>::gen();

  std::cerr << glGetString(GL_VERSION) << std::endl;
  glutMainLoop();
  return 0;
}


//Algorithm
std::vector<std::pair<int, int>>
sutherland(pt_t const &start, pt_t const &end,
           std::pair<pt_t, pt_t> clippedWindow) {
  std::array<std::bitset<4>, 9> partitions = {
      0b1001, 0b1000, 0b1010, 0b0001, 0b0000, 0b0010, 0b0101, 0b0100, 0b0110};
  std::array<std::bitset<4>, 2> labels = {0, 0};
  for (int i = 0; i < labels.size(); ++i) {
    pt_t const &pt = i ? start : end;
    if (pt.first < clippedWindow.first.first)
      labels[i][3] = 1;
    if (pt.first > clippedWindow.second.first)
      labels[i][2] = 1;
    if (pt.second < clippedWindow.first.second)
      labels[i][0] = 1;
    if (pt.second > clippedWindow.second.second)
      labels[i][1] = 1;
  }
  if ((labels[0] & labels[1]).any())
    return {};
  if ((labels[0] | labels[1]).none())
    return bresenham(start, end);
  return bresenham(getPointOnBound(start, clippedWindow),
                   getPointOnBound(end, clippedWindow));
}

``` 
*Output*:  

\newpage
